import React, {ChangeEvent, useState} from 'react';
import logo from './logo.svg';
import './App.css';
import {Card, Container, Grid} from "@material-ui/core";
import QuizQn from "./QuizQn";
import "./QuizQn.css"

const qns = [{gender: "female"}]

const App: React.FC = () => {
  const [form, setForm] = useState({gender: "female"})
  console.log(form);
  return (
      <Grid className={"App"}>
        { qns.map((qn) =>
          <QuizQn value={} onChange={(input: any) => setForm((prevState) => ({...prevState, gender: input.target.value}))}></QuizQn>
        )}
        {/*<QuizQn></QuizQn>*/}
        {/*<QuizQn></QuizQn>*/}
        {/*<QuizQn></QuizQn>*/}
        {/*<QuizQn></QuizQn>*/}
        {/*<QuizQn></QuizQn>*/}
      </Grid>
  );
}

export default App;

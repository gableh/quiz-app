import React, {ChangeEvent, ChangeEventHandler} from "react";
import {
    Card,
    Checkbox,
    Container, Divider,
    FormControl,
    FormControlLabel, FormLabel,
    Input, makeStyles,
    Radio,
    RadioGroup,
    Select
} from "@material-ui/core";
import "./QuizQn.css"
const QuizQn: React.FC<{onChange: any, value: string}> = (props) => {
    const classes = useStyles();

    return (
        <Card className={"Card"} raised={true}>
            <Container className={"QuizQn"}>
                <FormLabel>
                    What is this?
                </FormLabel>
                <Divider />
                <FormControl style={{flex: 1, width: "100%"}}>
                        <RadioGroup
                            style={{flex: 1}}
                            aria-label="Gender"
                            name="gender1"
                            value={props.value}
                            onChange={props.onChange}
                        >
                            <FormControlLabel className={classes.formControlLabel} value="female" control={<Radio color={"primary"}/>} label="Female" />
                            <FormControlLabel className={classes.formControlLabel} value="male" control={<Radio color={"primary"}/>} label="Male" />

                            <FormControlLabel className={classes.formControlLabel} value="other" control={<Radio color={"primary"}/>} label="Other" />
                            <FormControlLabel className={classes.formControlLabel}
                                value="disabled"
                                disabled
                                control={<Radio color={"primary"}/>}
                                label="(Disabl"
                            />
                        </RadioGroup>
                </FormControl>
            </Container>
        </Card>
    );
}

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    formControlLabel: {
        flex: 1
    },
    formControl: {
        margin: theme.spacing(3),
    },
    group: {
        margin: theme.spacing(1, 0),
    },
}));

export default QuizQn;